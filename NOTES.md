Candidate Name: Wilf Engel

Tasks: 2, 3

Time: 1:40, .45

Notes:

## Task 2

- Please look to `./routes/meter-readings-routes.js` for task application logic
- The project structure is inteded to seperate concerns where appropriate to enable these components to be reused. This is a HTTP service but we could easily hook up an event queue such as rabbitMQ or Kafka and reuse the same service methods for example to create meter readings from an event driven microservice
- Tests have been included covering the core functionality of the API. As integration tests, they are intended to cover the required application functions. An improvement would be to include a mocking library such as sinon to ensure full coverage with independent unit tests
- The tests cover the 'happy flow'. Again, we should use mocks to cover dependency errors.
- The logic assumes that the user is authenticated and that all the data belongs to and is accessible by them. Naturally, access should be limited to this users OWN meter readings.
- An additional endpoint to get readings by month has been added. Initially it was just for quick testing but it has been left there anyway... The business logic for this endpoint is rather vague ;-)
- TODOs have been left in some places as time was running out. I look forward to discussing the possible improvements.

## Task 3

- Please look to `./routes/monthly-usage-routes.js` for task application logic
- The task has been achieved by adding an additional route for handling monthly usage
- Monthly usage is calculated on the fly for all available readings - an improvement on this would be to add a db event listener to trigger a calculation when a new reading has been entered. This could then be stored along with the readings for fast retrieval
- Tests should cover the major functionalities although again, it would be benefitial to use proper mocks and unit tests for each of the services, stores etc.
