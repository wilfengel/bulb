const Koa = require('koa');
const KoaRouter = require('koa-router');
const bodyparser = require('koa-bodyparser');
const respond = require('koa-respond');
const cors = require('@koa/cors');

const { errorHandler } = require('./middleware/error-handler');

const data = require('./data');
const meterReadings = require('./routes/meter-readings-routes');
const monthlyUsage = require('./routes/monthly-usage-routes');

const PORT = process.env.PORT || 3000;

function createServer() {
  const server = new Koa();
  server.use(errorHandler);

  // Parse request body
  server.use(bodyparser());
  // Response utilities - ctx.ok()...
  server.use(respond());
  // Handle CORS
  server.use(cors());
  // TODO koa helmet or equivalent?

  const router = new KoaRouter();

  // Healthcheck route
  router.get('/', (ctx, next) => {
    ctx.body = 'Hello world';
    next();
  });

  // Application routes
  router.use(meterReadings.routes());
  router.use(monthlyUsage.routes());

  server.use(router.allowedMethods());
  server.use(router.routes());

  return server;
}

module.exports = createServer;

if (!module.parent) {
  data.initialize();
  const server = createServer();
  server.listen(PORT, () => {
    console.log(`server listening on port ${PORT}`);
  });
}
