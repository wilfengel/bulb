const moment = require('moment');

/**
 * Returns the difference between two moment objects in number of days.
 * @param {moment} mmt1
 * @param {moment} mm2
 */
function getDiffInDays(mmt1, mm2) {
  return mmt1.diff(mm2, 'days');
}

/**
 * Return the number of days between the given moment object
 * and the end of the month of this moment object.
 * @param {moment} mmt
 */
function getDaysUntilMonthEnd(mmt) {
  return getDiffInDays(moment.utc(mmt).endOf('month'), mmt);
}

/**
 * Validate date is usable
 * @param {string} date
 */
function validateDate(date) {
  return moment(date, 'YYYY-MM-DD', true).isValid();
}

/**
 * Get ISO date to search
 * @param {string date}
 */
function formatDate(date) {
  return moment.utc(date).toISOString();
}

module.exports = {
  getDiffInDays,
  getDaysUntilMonthEnd,
  validateDate,
  formatDate,
};
