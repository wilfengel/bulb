const Router = require('koa-router');
const monthlyUsageService = require('../services/monthly-usage-service');

const router = new Router({ prefix: '/monthly-usage' });

router.get('/', async ctx =>
  ctx.ok(await monthlyUsageService.find(ctx.query)));

module.exports = router;
