const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = require('chai');
const { beforeEach, afterEach } = require('mocha');

const expectedData = require('../../../sampleData.json').electricity;

chai.use(chaiHttp);

const server = require('../../index');

const prefix = 'meter-readings';

describe('meter readings http', () => {
  let instance;
  beforeEach(() => {
    instance = server().listen();
  });

  afterEach(() => {
    instance.close();
  });

  describe('/GET', () => {
    it('should respond successfully', (done) => {
      chai.request(instance)
        .get(`/${prefix}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          done();
        });
    });

    it('should respond successfully with the full list of readings', (done) => {
      chai.request(instance)
        .get(`/${prefix}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body.length).to.be.equal(expectedData.length);
          expect(res.body[0].cumulative).to.be.equal(expectedData[0].cumulative);
          expect(res.body[0].reading_date).to.be.equal(expectedData[0].readingDate);
          expect(res.body[0].unit).to.be.equal(expectedData[0].unit);
          done();
        });
    });
  });

  describe('/:date GET', () => {
    it('should respond successfully with 0 readings if the date is not matched', (done) => {
      const testDate = '2018-01-01'; // this date is not in the DB
      chai.request(instance)
        .get(`/${prefix}/${testDate}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body.length).to.be.equal(0);
          done();
        });
    });

    it('should respond successfully with 1 reading if the date is matched', (done) => {
      const testDate = '2018-04-29'; // this date has one reading

      const expectedReason = expectedData[expectedData.length - 1];
      chai.request(instance)
        .get(`/${prefix}/${testDate}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body.length).to.be.equal(1);
          expect(res.body[0].cumulative).to.be.equal(expectedReason.cumulative);
          done();
        });
    });

    // TODO seed DB with different data for this test
    it.skip('should respond successfully with 2 readings if the date is matched with two readings', (done) => {
      const testDate = '2018-04-29';

      const expectedReason = expectedData[expectedData.length - 1];
      chai.request(instance)
        .get(`/${prefix}/${testDate}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body.length).to.be.equal(1);
          expect(res.body[0].cumulative).to.be.equal(expectedReason.cumulative);
          done();
        });
    });

    it('should require the date to be provided in the correct format', (done) => {
      const testDate = '2018';

      chai.request(instance)
        .get(`/${prefix}/${testDate}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(500);
          expect(res.body.message).to.be.equal('Date format should be YYYY-MM-DD');
          done();
        });
    });
  });

  describe('/POST', () => {
    it('should respond with success when new reading is entered', (done) => {
      const reading = {
        cumulative: 10000,
        readingDate: '2018-01-01',
        unit: 'kWh',
      };

      chai.request(instance)
        .post(`/${prefix}/`)
        .send(reading)
        .end((err, res) => {
          expect(res.status).to.be.equal(201);
          expect(res.body.message).to.be.equal('Success');
          done();
        });
    });

    it('should reject readings with no input data', (done) => {
      const reading = {};

      chai.request(instance)
        .post(`/${prefix}/`)
        .send(reading)
        .end((err, res) => {
          expect(res.status).to.be.equal(500);
          expect(res.body.message).to.be.equal('Reading data required');
          done();
        });
    });

    it('should reject readings with invalid date', (done) => {
      const reading = {
        readingDate: 'All Hallows Eve',
      };

      chai.request(instance)
        .post(`/${prefix}/`)
        .send(reading)
        .end((err, res) => {
          expect(res.status).to.be.equal(500);
          expect(res.body.message).to.be.equal('Date format should be YYYY-MM-DD');
          done();
        });
    });
  });
});
