const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = require('chai');
const { beforeEach, afterEach } = require('mocha');

const meterReadings = require('../../../sampleData.json').electricity;

chai.use(chaiHttp);

const server = require('../../index');

const prefix = 'monthly-usage';

describe('monthly usage http', () => {
  let instance;
  beforeEach(() => {
    instance = server().listen();
  });

  afterEach(() => {
    instance.close();
  });

  describe('/GET', () => {
    it('should respond successfully', (done) => {
      chai.request(instance)
        .get(`/${prefix}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          done();
        });
    });

    it('should respond with the full list of readings', (done) => {
      chai.request(instance)
        .get(`/${prefix}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          // TODO reset db after each test to avoid this issue
          expect(res.body.length).to.be.equal(meterReadings.length + 1);
          done();
        });
    });

    it('should attach end of month only for the first reading', (done) => {
      const expectedEndOfMonth = 17609.833333333332;

      chai.request(instance)
        .get(`/${prefix}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body[0].endOfMonthReading).to.be.equal(expectedEndOfMonth);
          expect(res.body[0].monthUsage).to.be.null;
          done();
        });
    });

    it('should not attach usage or end of month for last reading', (done) => {
      chai.request(instance)
        .get(`/${prefix}`)
        .end((err, res) => {
          expect(res.status).to.be.equal(200);
          expect(res.body[res.body.length - 1].endOfMonthReading).to.be.undefined;
          expect(res.body[res.body.length - 1].monthUsage).to.be.undefined;
          done();
        });
    });

    it('should attach the correct calculations to the reading entry', (done) => {
      const expectedMonth2 = {
        cumulative: 17759,
        reading_date: '2017-04-15T00:00:00.000Z',
        unit: 'kWh',
        endOfMonthReading: 17917.478260869564,
        monthUsage: 307.644927536232,
      };
      chai.request(instance)
        .get(`/${prefix}`)
        .end((err, res) => {
          expect(res.body[1].endOfMonthReading).to.be.equal(expectedMonth2.endOfMonthReading);
          expect(res.body[1].monthUsage).to.be.equal(expectedMonth2.monthUsage);
          done();
        });
    });
  });
});
