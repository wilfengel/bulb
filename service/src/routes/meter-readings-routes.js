const Router = require('koa-router');
const meterReadingsService = require('../services/meter-readings-service');

const router = new Router({ prefix: '/meter-readings' });

router.get('/', async ctx =>
  ctx.ok(await meterReadingsService.find()));

router.get('/:date', async ctx =>
  ctx.ok(await meterReadingsService.get(ctx.params.date)));

router.post('/', async ctx =>
  ctx.created(await meterReadingsService.create(ctx.request.body)));

// router.patch(':id', async ctx =>
//  ctx.ok(await meterReadingsService.update(ctx.params.id, ctx.request.body)));

// router.delete('/:id', async ctx =>
//  ctx.noContent(await meterReadingsService.delete(ctx.params.id)));

module.exports = router;
