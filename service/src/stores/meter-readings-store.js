const { connection } = require('../data');

/**
 * return all meter_reads
 */
async function find() {
  const result = await new Promise((resolve, reject) => {
    connection.all('SELECT * FROM meter_reads', (error, data) => {
      if (error) {
        return reject(error);
      }

      return resolve(data);
    });
  });
  return result;
}

/**
 * return meter_reads matching a specific date
 */
async function get(date) {
  // TODO prefer searching by date range (on same day)
  const result = await new Promise((resolve, reject) => {
    connection.all('SELECT * FROM meter_reads where reading_date = ?', date, (error, data) => {
      if (error) {
        return reject(error);
      }

      return resolve(data);
    });
  });
  return result;
}

/**
 * insert new meter_reads
 */
async function create(reading) {
  const { cumulative, readingDate, unit } = reading;
  const result = await new Promise((resolve, reject) => {
    connection.run(
      'INSERT INTO meter_reads (cumulative, reading_date, unit) VALUES (?, ?, ?)',
      [cumulative, readingDate, unit], (error) => {
        if (error) {
          return reject(error);
        }

        return resolve('Success');
      },
    );
  });
  return result;
}

module.exports = {
  find,
  get,
  create,
};
