/**
* Error Handler middleware
* Print err if available
* TODO - Send to service eg. loggly
*/
async function errorHandler(ctx, next) {
  try {
    await next();
  } catch (err) {
    ctx.status = err.statusCode || 500;
    ctx.body = err.toJSON ? err.toJSON() : { message: err.message, ...err };
    if (process.env.NODE_ENV === 'dev') {
      console.error({ msg: 'Error in request', err });
    }
  }
}

module.exports = {
  errorHandler,
};
