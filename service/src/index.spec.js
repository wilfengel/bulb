const Koa = require('koa');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { expect } = require('chai');
const { beforeEach, afterEach } = require('mocha');

chai.use(chaiHttp);

const server = require('./index');

describe('index initialise', () => {
  let instance;
  beforeEach(() => {
    instance = server();
  });

  it('should create an instance of a Koa server', () => {
    expect(instance).to.be.instanceof(Koa);
  });

  it('should attach the right number of middlewares', () => {
    // Count of server.use()
    const expectedMiddlewares = 6;
    expect(instance.middleware.length).to.be.eq(expectedMiddlewares);
  });
});

describe('index http', () => {
  let instance;
  beforeEach(() => {
    instance = server().listen();
  });

  afterEach(() => {
    instance.close();
  });

  describe('/GET', () => {
    it('should respond successfully', (done) => {
      chai.request(instance)
        .get('/')
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.text).to.be.equal('Hello world');
          done();
        });
    });

    it('should handle not found routes correctly', (done) => {
      chai.request(instance)
        .get('/xsr900')
        .end((err, res) => {
          expect(res).to.have.status(404);
          expect(res.text).to.be.equal('Not Found');
          done();
        });
    });

    // TODO test for POST, PUT etc. unavailable
  });
});
