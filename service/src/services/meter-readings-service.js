const meterReadingStore = require('../stores/meter-readings-store');
const { validateDate, formatDate } = require('../libs/moment-lib');

/**
 * return all entries in the store
 */
async function find() {
  return meterReadingStore.find();
}

/**
 * return entries matching a specified date
 */
async function get(date) {
  if (!date) {
    throw new Error('No date param');
  }

  if (!validateDate(date)) {
    throw new Error('Date format should be YYYY-MM-DD');
  }

  return meterReadingStore.get(formatDate(date));
}

/**
 * create a new reading entry in the store
 */
async function create(input) {
  if (Object.keys(input).length === 0) {
    throw new Error('Reading data required');
  }

  const newReading = {
    unit: input.unit,
    cumulative: input.cumulative,
  };

  if (!validateDate(input.readingDate)) {
    throw new Error('Date format should be YYYY-MM-DD');
  } else {
    newReading.readingDate = formatDate(input.readingDate);
  }

  // TODO validate reading units and number

  return meterReadingStore.create(newReading);
}

module.exports = {
  find,
  get,
  create,
};
