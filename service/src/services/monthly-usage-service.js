const meterReadingStore = require('../stores/meter-readings-store');
const { getDaysUntilMonthEnd, getDiffInDays } = require('../libs/moment-lib');
const moment = require('moment');


async function getAllReadings() {
  return meterReadingStore.find();
}

/**
 * Return the calculated end of month reading
 * for reading1 based on the difference with reading2
 * @param reading1
 * @param reading2
 */
function getEndOfMonthReading(reading1, reading2) {
  const daysToEndMonth = getDaysUntilMonthEnd(moment.utc(reading1.reading_date));
  const daysToNextReading = getDiffInDays(
    moment.utc(reading2.reading_date),
    moment.utc(reading1.reading_date),
  );
  const dailyUsage = (reading2.cumulative - reading1.cumulative) / daysToNextReading;
  return reading1.cumulative + (dailyUsage * daysToEndMonth);
}

/**
 * Return the readings array with calculated end
 * of month readings and the month usage
 * @param readingsArray
 */
function mapReadingsToUsage(readingsArray) {
  const processed = [];
  readingsArray.forEach((reading, i) => {
    // Do not interpolate at outer edge
    if (i + 1 !== readingsArray.length) {
      const endOfMonthReading = getEndOfMonthReading(reading, readingsArray[i + 1]);

      let monthUsage = null;

      // Do not interpolate inner edge
      if (i !== 0) {
        monthUsage = endOfMonthReading - processed[i - 1].endOfMonthReading;
      }
      processed.push({ ...reading, endOfMonthReading, monthUsage });
    } else {
      processed.push(reading);
    }
  });

  return processed;
}

async function find() {
  // Get sorted list of readings
  const readings = await getAllReadings();

  // Calculate usage
  const readingsWithUsage = mapReadingsToUsage(readings);
  return readingsWithUsage;
}

module.exports = {
  find,
};
